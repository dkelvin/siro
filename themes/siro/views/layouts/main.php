<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/bootstrap/bootstrap.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/jquery-ui/jquery-ui.min.css" media="screen, projection" />
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
            <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/fancybox/jquery.fancybox.css" media="print" />
                <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/fullcalendar/fullcalendar.css" media="print" />
                <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/xcharts/xcharts.min.css" media="print" />
                <!--[if lt IE 8]>
                <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
                <![endif]-->

                <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/select2/select2.css" />
                <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />

                <title><?php echo CHtml::encode($this->pageTitle); ?></title>
                </head>

                <!--Start Header-->
                <div id="screensaver">
                    <canvas id="canvas"></canvas>
                    <i class="fa fa-lock" id="screen_unlock"></i>
                </div>
                <div id="modalbox">
                    <div class="devoops-modal">
                        <div class="devoops-modal-header">
                            <div class="modal-header-name">
                                <span>Basic table</span>
                            </div>
                            <div class="box-icons">
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="devoops-modal-inner">
                        </div>
                        <div class="devoops-modal-bottom">
                        </div>
                    </div>
                </div>
                <header class="navbar">
                    <div class="container-fluid expanded-panel">
                        <div class="row">
                            <div id="logo" class="col-xs-12 col-sm-2">
                                <a href="index.html">Siro</a>
                            </div>
                            <div id="top-panel" class="col-xs-12 col-sm-10">
                                <div class="row">
                                    <div class="col-xs-8 col-sm-4">
                                        <a href="#" class="show-sidebar">
                                            <i class="fa fa-bars"></i>
                                        </a>

                                    </div>
                                    <div class="col-xs-4 col-sm-8 top-panel-right">
                                        <ul class="nav navbar-nav pull-right panel-menu">
                                            <li class="hidden-xs">
                                                <a href="index.html" class="modal-link">
                                                    <i class="fa fa-bell"></i>
                                                    <span class="badge">7</span>
                                                </a>
                                            </li>

                                            <li class="hidden-xs">
                                                <a href="ajax/page_messages.html" class="ajax-link">
                                                    <i class="fa fa-envelope"></i>
                                                    <span class="badge">7</span>
                                                </a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle account" data-toggle="dropdown">
                                                    <div class="avatar">
                                                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/avatar.jpg" class="img-rounded" alt="avatar" />
                                                    </div>
                                                    <i class="fa fa-angle-down pull-right"></i>
                                                    <div class="user-mini pull-right">
                                                        <span class="welcome">Welcome,</span>
                                                        <span>Jane Devoops</span>
                                                    </div>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-user"></i>
                                                            <span>Profile</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="ajax/page_messages.html" class="ajax-link">
                                                            <i class="fa fa-envelope"></i>
                                                            <span>Messages</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="ajax/gallery_simple.html" class="ajax-link">
                                                            <i class="fa fa-picture-o"></i>
                                                            <span>Albums</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="ajax/calendar.html" class="ajax-link">
                                                            <i class="fa fa-tasks"></i>
                                                            <span>Tasks</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-cog"></i>
                                                            <span>Settings</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-power-off"></i>
                                                            <span>Logout</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <!--End Header-->
                <!--Start Container-->
                <div id="main" class="container-fluid">
                    <div class="row">
                        <div id="sidebar-left" class="col-xs-2 col-sm-2">
                            <ul class="nav main-menu">
                                <li>
                                    <a href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/dashboard.html" class="active ajax-link">
                                        <i class="fa fa-dashboard"></i>
                                        <span class="hidden-xs">Bandeja de Entrada</span>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="fa fa-bar-chart-o"></i>
                                        <span class="hidden-xs">Reputación</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/charts_xcharts.html">Comentarios de tus Compradores</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/charts_flot.html">Comentarios de Vendedores</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="fa fa-table"></i>
                                        <span class="hidden-xs">Calificar Usuario</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/tables_simple.html">Simple Tables</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/tables_datatables.html">Data Tables</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/tables_beauty.html">Beauty Tables</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="fa fa-pencil-square-o"></i>
                                        <span class="hidden-xs">Forms</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/forms_elements.html">Elements</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/forms_layouts.html">Layouts</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/forms_file_uploader.html">File Uploader</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="fa fa-desktop"></i>
                                        <span class="hidden-xs">UI Elements</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/ui_grid.html">Grid</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/ui_buttons.html">Buttons</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/ui_progressbars.html">Progress Bars</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/ui_jquery-ui.html">Jquery UI</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/ui_icons.html">Icons</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="fa fa-list"></i>
                                        <span class="hidden-xs">Pages</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_login.html">Login</a></li>
                                        <li><a href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_register.html">Register</a></li>
                                        <li><a id="locked-screen" class="submenu" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_locked.html">Locked Screen</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_contacts.html">Contacts</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_feed.html">Feed</a></li>
                                        <li><a class="ajax-link add-full" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_messages.html">Messages</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_pricing.html">Pricing</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_invoice.html">Invoice</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_search.html">Search Results</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_404.html">Error 404</a></li>
                                        <li><a href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/page_500.html">Error 500</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="fa fa-map-marker"></i>
                                        <span class="hidden-xs">Maps</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/maps.html">OpenStreetMap</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/map_fullscreen.html">Fullscreen map</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="fa fa-picture-o"></i>
                                        <span class="hidden-xs">Gallery</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/gallery_simple.html">Simple Gallery</a></li>
                                        <li><a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/gallery_flickr.html">Flickr Gallery</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/typography.html">
                                        <i class="fa fa-font"></i>
                                        <span class="hidden-xs">Typography</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="ajax-link" href="<?php echo Yii::app()->theme->baseUrl; ?>/ajax/calendar.html">
                                        <i class="fa fa-calendar"></i>
                                        <span class="hidden-xs">Calendar</span>
                                    </a>
                                </li>

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="fa fa-picture-o"></i>
                                        <span class="hidden-xs">Multilevel menu</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">First level menu</a></li>
                                        <li><a href="#">First level menu</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle">
                                                <i class="fa fa-plus-square"></i>
                                                <span class="hidden-xs">Second level menu group</span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Second level menu</a></li>
                                                <li><a href="#">Second level menu</a></li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle">
                                                        <i class="fa fa-plus-square"></i>
                                                        <span class="hidden-xs">Three level menu group</span>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Three level menu</a></li>
                                                        <li><a href="#">Three level menu</a></li>
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle">
                                                                <i class="fa fa-plus-square"></i>
                                                                <span class="hidden-xs">Four level menu group</span>
                                                            </a>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#">Four level menu</a></li>
                                                                <li><a href="#">Four level menu</a></li>
                                                                <li class="dropdown">
                                                                    <a href="#" class="dropdown-toggle">
                                                                        <i class="fa fa-plus-square"></i>
                                                                        <span class="hidden-xs">Five level menu group</span>
                                                                    </a>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#">Five level menu</a></li>
                                                                        <li><a href="#">Five level menu</a></li>
                                                                        <li class="dropdown">
                                                                            <a href="#" class="dropdown-toggle">
                                                                                <i class="fa fa-plus-square"></i>
                                                                                <span class="hidden-xs">Six level menu group</span>
                                                                            </a>
                                                                            <ul class="dropdown-menu">
                                                                                <li><a href="#">Six level menu</a></li>
                                                                                <li><a href="#">Six level menu</a></li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="#">Three level menu</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--Start Content-->
                        <div id="content" class="col-xs-12 col-sm-10">
                            <!--			<div class="preloader">
                                                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/devoops_getdata.gif" class="devoops-getdata" alt="preloader"/>
                                                    </div>-->
                            <div id="ajax-content"> <?php echo $content; ?></div>
                        </div>
                        <!--End Content-->
                    </div>
                </div>
                <!--End Container-->
                <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                <!--<script src="http://code.jquery.com/jquery.js"></script>-->
                <script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/jquery/jquery-2.1.0.min.js"></script>
                <script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
                <!-- Include all compiled plugins (below), or include individual files as needed -->
                <script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/bootstrap/bootstrap.min.js"></script>
                <script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/justified-gallery/jquery.justifiedgallery.min.js"></script>
                <script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/tinymce/tinymce.min.js"></script>
                <script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/tinymce/jquery.tinymce.min.js"></script>
                <!-- All functions for this theme + document.ready processing -->
                <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/devoops.js"></script>
                </body>
                </html>
